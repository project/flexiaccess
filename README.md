# Contents of this file

* [Introduction](#intro)
* [Requirements](#requirements)
* [Recommended modules](#recommended)
* [Installation](#install)
* [Configuration](#config)
* [Permissions](#perms)
* [Usage notes](#usage)
* [Troubleshooting](#trouble)
* [Maintainer](#maintainers)


# Introduction<a id="intro"></a>

**Flexi access** provides a simple interface to the **ACL** (Access
Control List) module. It lets you set up and mange ACLs naming
individual users that are allowed access to a particular node.

The idea behind **Flexi access** is to allow per user acces control
for a node without the complexity (or features) of [**Organic groups**][1],
and without having to create a lot of (overlapping) roles.

* For a full description of the module, visit the [project page][2].
* For community documentation, visit the [documentation page][3].
* To submit bug reports and feature suggestions, or to track changes,
  use the [issue queue][4].


# Requirements<a id="requirements"></a>

* [ACL][5]:  
  The module that manages the access lists.


# Recommended modules<a id="recommended"></a>

* [Advanced Help][6]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `help/flexiaccess/README.md`.
* [Markdown][7]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered with the markdown filter.


# Installation<a id="install"></a>

First, unless you've already done so, download, install [**ACL**][5].

1. Install **Flexi access** as you would normally install a
   contributed drupal module. See: [Installing modules][8] for further
   information.
2. Enable **Flexi access**.
3. You'll get a message telling you to rebuild permissions. Do it!
4. **Flexi access is** now ready for use.

However, it will not do anything until you set up **Flexi access** to
mange at least one content type and create an ACL for at least one
node.

# Configuration<a id="config"></a>

The administrative interface (*Administration » Configuration » People
» Flexi access*) has the following tabs:

* Main settings
* Anonymous user:
* Bulk operations:

## Main settings

**Content types:**  

This lets you enable or disable the content types you want **Flexi
access** to manage (e.g. “Article”).

**Per-type settings:**  

Enabling automatic ACL creation for a content type will make access to
newly created nodes restricted by default. Enabling this will create
an empty ACL for every new node created, thereby restricting access to
the node by all users until they're added to the node's ACL. This does
not affect existing nodes.

**Default priority for new ACL entries:**

The default priority of **Flexi access** is zero (<code>0</code>).
This is the also the default priority for other node access modules.

Node access modules may set their priorities higher or lower than
zero.  If another module has lower priority than **Flexi access** for
a given node, its grants will be ignored.  Conversely, if any module
returns a higher priority a node, the grants of **Flexi access** will
be ignored.


## Anonymous user

Allow administrator to store a string in the database as the name for
the Anonymous user (user #0). To be able to attach an ACL to the
anonymous user, this string cannot be empty.

## Bulk operations

Bulk operations to reset ACL priorities and delete all ACLs.


# Permissions<a id="perms"></a>

To set permissions, navigate to *Administration » People »
Permissions*, and scroll down to the Flexi access section.

The following permissions exist:

* Administer Flexi access  
  This permission grants the user access to the Flexi access
  administrative interface, including the right to administer ACLs via
  the user profiles. Warning: Give to trusted roles only; this
  permission has security implications.

* Administer all nodes  
  This permission grants the user the ability to administer all nodes
  of a type enabled for **Flexi acess**.  Warning: Give to trusted
  roles only; this permission has security implications.

* Administer own nodes
  This permission grants the user the ability to administer own nodes
  of a type enabled for **Flexi acess**.

* Create ACLs  
  This permission grants the user the ability to create ACLs on all
  the managed nodes the user has been granted the right to administer.

* Grant view permission  
  This permission grants the user the ability to grant or withdraw
  view pernission on all the managed nodes the user has been granted
  the right to administer.

* Grant update permission  
  This permission grants the user the ability to grant or withdraw
  edit or update pernission on all the managed nodes the user has been
  granted the right to administer.

* Grant delete permission  
  This permission grants the user the ability to grant or withdraw
  delete pernission on all the managed nodes the user has been granted
  the right to administer.

The last four (“Create ACLs” the the CRUD permissions) only applies to
the nodes selected by the “Administer all/own nodes” permissions.


# Usage notes<a id="usage"></a>

If a user has been granted access to a node by this module, the user
will be given access to the node, even if the node is unpublished.
The use cases for this is to give users permission to review and
update nodes prior to publication.

When a user granted the “Adminster all nodes” permission views a node,
or a user granted the “Administer own nodes” permission views an own
node, and there exist CRUD permissions for a user of that role, that
user will see a tab called “Flexi access”.

Provided the user belongs to a role with the required permissions,
clicking on the “Flexi access”-tab for a node that has no ACL
associated, will show a button to create an ACL.  Creating an ACL
will automatically add the user creating the node to the ACL with
all the CRUD permissions the user has been granted

Until an ACL is
created, access will not be managed by **Flexi access**.  When an ACL
is created, access will be managed by **Flexi access** and only the
users named in the node's ACL granted the View right will be able to
see the node.

When **Flexi access** manages a node, there will be up to three
subfields (view, update, delete) inside the “Flexi access” tab. The
number of visible subfields depends on what permissions the user are
granted.

A user granted the “Administer Flexi access” permission who views a
user profile will see a tab named “Flexi access”.  This tab gives
access to a page for simple management of ACLs for that particular
user.


# Troubleshooting<a id="trouble"></a>

If the “Flexi access” tab does not appear when a user belonging to a
role is granted the right to edit the node, it is beacuse no valid
permissions exists for the role on that node.  As a minimum the
permission “Administer own nodes” and at least one of the CRUD
permissions must be given to the user.

If you can no longer access a node you own or manage, it is usually
because you've locked yourself out by creating an ACL for the node
without granting yourself access.  If this happens, ask the
super-admin to add you to the node's ACL.

If you're using both the **Flexi access** and the **Content access**
module, and you enable the per node access control settings for a
content type for the latter, you will have access to two almost
identical interfaces for per user access control.  This is usually
quite confusing, and the settings may even be conflicting.  It is
better not do do this.

Using **Flexi access** with other content access modules may create
conflicts between the modules.  Drupal node access module should only
grant access to content nodes, not deny it. By default, if you enable
several (well-behaved) node access modules, access will be granted as
soon as at least one of the realms with the highest priority grants
it. Note that if there is more than one realm with the same priority,
all are consdidered.

It is possible for access control modules to specify what the priority
of their grants should be.  (Developers may wish to look at the API
documentation of [`node_access_acquire_grants()`][9] and
[`hook_node_access_records()`][10] for more information.)  **TL;DR**:
The node access system looks at only the *highest* priority grants
when determining node access.

You should also be aware of this: Some Drupal node access module may
not be well-behaved.  If you use more than one node access module on a
site, conflicts between them may be hard to sort out.  The **Devel
DNA** sub-module may be useful if you need to debug node access
conflicts.

# Maintainers<a id="maintainers"></a>

**Flexi access** was first created by [good_man][11] (Khaled Al Hourani).  
The current maintainer is [gisle][12] (Gisle Hannemyr).  
This project has been sponsored by [Hannemyr Nye Medier AS][13].

Any help with development (patches, reviews, comments) are welcome.

[1]: https://www.drupal.org/project/og
[2]: https://www.drupal.org/project/flexiaccess
[3]: https://www.drupal.org/docs/7/modules/flexi-access
[4]: https://www.drupal.org/project/issues/flexiaccess
[5]: https://www.drupal.org/project/acl
[6]: https://www.drupal.org/project/advanced_help
[7]: https://www.drupal.org/project/markdown
[8]: https://www.drupal.org/docs/7/extend/installing-modules
[9]: https://api.drupal.org/api/drupal/modules%21node%21node.module/function/node_access_acquire_grants/7.x
[10]: https://api.drupal.org/api/drupal/modules%21node%21node.api.php/function/hook_node_access_records/7.x
[11]: https://www.drupal.org/u/good_man
[12]: https://www.drupal.org/u/gisle
[13]: https://hannemyr.no
